import React, { lazy, Suspense } from "react";
import { Route, Switch } from "react-router-dom";

const RoomViewComponent = lazy(() => import("./RoomView/RoomView"));
const HomePageComponent = lazy(() => import("./HomePage/HomePage"));
const TestPageComponent = lazy(() => import("./TestPage"));

export const RouterComponent: React.FC = () => {
    return (
        <Suspense fallback={<></>}>
            <Switch>
                <Route path="/" exact>
                    <HomePageComponent />
                </Route>

                <Route path="/test" exact>
                    <TestPageComponent />
                </Route>

                <Route path="/room/:id?">
                    <RoomViewComponent />
                </Route>
            </Switch>
        </Suspense>
    );
};

export default RouterComponent;
