import React from "react";
import { CssBaseline } from "@material-ui/core";
import { BrowserRouter as Router } from "react-router-dom";
import RouterComponent from "./Router";
import TopBarComponent from "./TopBar/TopBar";
import "./App.css";

export const App: React.FC = () => {
    return (
        <>
            <CssBaseline />
            <div className="App">
                <Router basename={process.env.PUBLIC_URL}>
                    <TopBarComponent />

                    <RouterComponent />
                </Router>
            </div>
        </>
    );
};

export default App;
