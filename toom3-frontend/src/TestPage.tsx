import React, { createRef, MutableRefObject, useRef, useState } from "react";
import { Command, IWSMessage } from "./utils/ws-messages";
import { iceServers } from "./utils/webrtc-configs.json";
import { wsUrl } from "./utils/ws-url";

interface WebRtcConnection {
    peerConnection?: RTCPeerConnection;
    remoteVideoTrack?: MediaStreamTrack;
    remoteAudioTrack?: MediaStreamTrack;
    localVideoTrack?: MediaStreamTrack;
    localAudioTrack?: MediaStreamTrack;
    remoteStream?: MediaStream;
    videoRef?: MutableRefObject<HTMLVideoElement | null>;
}

export const TestPageComponent: React.FC = () => {
    const ws = useRef<WebSocket | null>(null);
    const [userUUID, setUserUUID] = useState("");
    const [roomUUID, setRoomUUID] = useState("");
    const [participants, setParticipants] = useState<string[]>([]);
    const webRtcConnections = useRef(
        {} as {
            [key: string]: WebRtcConnection;
        }
    );

    const createWS = () => {
        ws.current = new WebSocket(wsUrl());
    };

    const listenForParticipantsUpdates = () => {
        ws.current!.addEventListener("message", (e) => {
            const message = JSON.parse(e.data) as IWSMessage;
            if (message.command === "UPDATE_PARTICIPANTS") {
                for (const participant of message.args["participants"]) {
                    if (!webRtcConnections.current[participant]) {
                        webRtcConnections.current[participant] = {
                            videoRef: createRef(),
                        };
                    }
                }
                setParticipants(() => message.args["participants"]);
            }
        });
    };
    const waitForResponse = async (
        message: IWSMessage,
        expectedReturnCommand: Command
    ): Promise<IWSMessage> => {
        return await new Promise((r) => {
            const handler = (e: MessageEvent) => {
                const reply = JSON.parse(e.data) as IWSMessage;
                if (reply.command === expectedReturnCommand) {
                    r(reply);
                    ws.current!.removeEventListener("message", handler);
                }
            };
            ws.current!.addEventListener("message", handler);
            ws.current!.send(JSON.stringify(message));
        });
    };

    const getUserUUID = async () => {
        const c1 = {
            command: "REQUEST_UUID",
        } as IWSMessage;
        const r1: IWSMessage = await waitForResponse(c1, "ISSUE_USER_UUID");
        const uuid = r1.args["uuid"] as string;
        setUserUUID(uuid);
    };

    const createRoom = async () => {
        const message: IWSMessage = {
            command: "REQUEST_CREATE_ROOM",
            args: {
                userUUID: userUUID,
            },
        };
        const r1: IWSMessage = await waitForResponse(
            message,
            "ISSUE_ROOM_UUID"
        );
        const roomUUID = r1.args["uuid"] as string;
        setRoomUUID(roomUUID);
    };

    const joinRoom = async () => {
        const message: IWSMessage = {
            command: "REQUEST_JOIN_ROOM",
            args: {
                userUUID: userUUID,
                roomUUID: roomUUID,
            },
        };
        const r1: IWSMessage = await waitForResponse(
            message,
            "JOIN_ROOM_RESPONSE"
        );
        const { success, err } = r1.args;
        if (success) {
            alert("joined room");
            setRoomUUID(() => r1.args["roomUUID"]);
        } else {
            alert(`error: ${err}`);
        }
    };

    const requestParticipants = async () => {
        const message: IWSMessage = {
            command: "REQUEST_PARTICIPANTS",
            args: {
                userUUID: userUUID,
            },
        };
        ws.current!.send(JSON.stringify(message));
    };

    const listenForWebrtcOffer = () => {
        ws.current!.addEventListener("message", async (e) => {
            const message = JSON.parse(e.data) as IWSMessage;
            if (message.command === "WEB_RTC_OFFER") {
                const connection = new RTCPeerConnection({ iceServers });
                const { offer, senderUUID } = message.args;

                const video = await navigator.mediaDevices.getUserMedia({
                    video: true,
                });
                const track = video.getVideoTracks()[0];
                connection.addTrack(track);

                connection.onconnectionstatechange = (e) => {
                    console.log(e);
                    webRtcConnections.current[senderUUID] = {
                        ...webRtcConnections.current[senderUUID],
                        peerConnection: connection,
                    };
                };
                connection.ontrack = (e) => {
                    console.log(e);
                    const webrtcConnection =
                        webRtcConnections.current[senderUUID];
                    const track = e.track;

                    console.log(track);

                    const stream = new MediaStream();
                    stream.addTrack(track);
                    webrtcConnection.remoteStream = stream;
                    webrtcConnection.remoteVideoTrack = track;
                    webrtcConnection.videoRef!.current!.srcObject = stream;
                    webrtcConnection.videoRef!.current!.play();
                };
                console.log(connection);

                await connection.setRemoteDescription(offer);
                const answer = await connection.createAnswer();

                await connection.setLocalDescription(answer);
                const answerMessage: IWSMessage = {
                    command: "WEB_RTC_ANSWER",
                    args: {
                        senderUUID: userUUID,
                        receiverUUID: senderUUID,
                        answer: answer,
                    },
                };
                await waitForResponse(answerMessage, "WEB_RTC_ANSWER_RESPONSE");
            }
        });
    };

    const connect = async (peerUUID: string) => {
        const connection = new RTCPeerConnection({ iceServers });
        connection.onconnectionstatechange = (e) => {
            console.log(e);
            webRtcConnections.current[peerUUID] = {
                ...webRtcConnections.current[peerUUID],
                peerConnection: connection,
            };
        };
        connection.ontrack = (e) => {
            console.log(e);
            const webrtcConnection = webRtcConnections.current[peerUUID];
            const track = e.track;

            console.log(track);

            const stream = new MediaStream();
            stream.addTrack(track);
            webrtcConnection.remoteStream = stream;
            webrtcConnection.remoteVideoTrack = track;
            webrtcConnection.videoRef!.current!.srcObject = stream;
            webrtcConnection.videoRef!.current!.play();
        };

        connection.addEventListener("track", (e) => {
            console.log(e);
        });

        const video = await navigator.mediaDevices.getUserMedia({
            video: true,
            audio: true,
        });
        const track = video.getVideoTracks()[0];
        connection.addTrack(track);

        const offer = await connection.createOffer();
        await connection.setLocalDescription(offer);
        const realOffer = await new Promise((r) => {
            console.log(connection);
            connection.onicecandidate = (e) => {
                console.log(e);
                if (!e.candidate) {
                    console.log("done");
                    console.log(connection.localDescription?.sdp);
                    r(connection.localDescription);
                }
            };
        });
        // const realOffer = connection.localDescription;

        const message: IWSMessage = {
            command: "WEB_RTC_OFFER",
            args: {
                senderUUID: userUUID,
                receiverUUID: peerUUID,
                offer: realOffer,
            },
        };
        const reply = await waitForResponse(message, "WEB_RTC_ANSWER");
        const { answer } = reply.args;
        console.log(answer);
        console.log(connection);
        await connection.setRemoteDescription(answer);
    };

    const leaveRoom = async () => {
        const message: IWSMessage = {
            command: "REQUEST_LEAVE_ROOM",
            args: {
                userUUID: userUUID,
            },
        };
        const reply = await waitForResponse(message, "LEAVE_ROOM_RESPONSE");
        const { success, err } = reply.args;
        if (success) {
            alert("left room");
        } else {
            alert(`error: ${err}`);
        }
    };

    return (
        <div className="TestPage">
            <h1>Toom3</h1>
            <button onClick={() => createWS()}>Create WS</button>
            <button onClick={() => listenForParticipantsUpdates()}>
                Listen for participants updates
            </button>
            <button onClick={() => listenForWebrtcOffer()}>
                Listen for webrtc offer
            </button>
            <table>
                <thead>
                    <tr>
                        <th>Participant</th>
                        <th>Connect</th>
                        <th>Video</th>
                    </tr>
                </thead>
                <tbody>
                    {participants.map((participant, i) => {
                        return (
                            <tr key={i}>
                                <td>{participant}</td>
                                <td>
                                    <button
                                        onClick={() => connect(participant)}
                                    >
                                        Connect
                                    </button>
                                </td>
                                <tr>
                                    <video
                                        style={{ width: 500 }}
                                        ref={
                                            webRtcConnections.current[
                                                participant
                                            ].videoRef
                                        }
                                        autoPlay
                                        muted
                                    ></video>
                                </tr>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <br />
            <br />
            <button onClick={() => getUserUUID()}>Get User UUID</button>
            <p>{userUUID}</p>
            <br />
            <br />
            <input
                type="text"
                value={roomUUID}
                onChange={(e) => setRoomUUID(() => e.target.value)}
            ></input>
            <button onClick={() => createRoom()}>Create Room</button>
            <button onClick={() => joinRoom()}>Join Room</button>
            <button onClick={() => requestParticipants()}>
                Request Particiants
            </button>
            <br />
            <br />
            <button onClick={() => leaveRoom()}>Leave Room</button>
        </div>
    );
};

export default TestPageComponent;
