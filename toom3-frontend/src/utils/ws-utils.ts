import { Command, IWSMessage } from "./ws-messages";

export const waitForResponse = async (
    ws: WebSocket,
    message: IWSMessage,
    expectedReturnCommand: Command
): Promise<IWSMessage> => {
    return await new Promise((r) => {
        const handler = (e: MessageEvent) => {
            const reply = JSON.parse(e.data) as IWSMessage;
            if (reply.command === expectedReturnCommand) {
                r(reply);
                ws.removeEventListener("message", handler);
            }
        };
        ws.addEventListener("message", handler);
        ws.send(JSON.stringify(message));
    });
};

export const getUserUUID = async (ws: WebSocket): Promise<string> => {
    const c1 = {
        command: "REQUEST_UUID",
    } as IWSMessage;
    const r1: IWSMessage = await waitForResponse(ws, c1, "ISSUE_USER_UUID");
    const uuid = r1.args["uuid"] as string;
    return uuid;
};

export const createRoom = async (
    ws: WebSocket,
    userUUID: string
): Promise<string> => {
    const message: IWSMessage = {
        command: "REQUEST_CREATE_ROOM",
        args: {
            userUUID: userUUID,
        },
    };
    const r1: IWSMessage = await waitForResponse(
        ws,
        message,
        "ISSUE_ROOM_UUID"
    );
    const roomUUID = r1.args["uuid"] as string;
    return roomUUID;
};

export const leaveRoom = async (ws: WebSocket, userUUID: string) => {
    const message: IWSMessage = {
        command: "REQUEST_LEAVE_ROOM",
        args: {
            userUUID: userUUID,
        },
    };
    const reply = await waitForResponse(ws, message, "LEAVE_ROOM_RESPONSE");
    const { success, err } = reply.args;
    if (!success) {
        throw err;
    }
};

export const joinRoom = async (
    ws: WebSocket,
    userUUID: string,
    roomUUID: string
): Promise<string> => {
    const message: IWSMessage = {
        command: "REQUEST_JOIN_ROOM",
        args: {
            userUUID: userUUID,
            roomUUID: roomUUID,
        },
    };
    const r1: IWSMessage = await waitForResponse(
        ws,
        message,
        "JOIN_ROOM_RESPONSE"
    );
    const { success, err } = r1.args;
    if (!success) {
        throw err;
    }
    return r1.args["roomUUID"] as string;
};
