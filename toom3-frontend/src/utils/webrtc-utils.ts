import { createRef, MutableRefObject } from "react";
import configs from "./webrtc-configs.json";
import { IWSMessage } from "./ws-messages";
import { waitForResponse } from "./ws-utils";

export interface WebRtcConnection {
    peerConnection: RTCPeerConnection;
    remoteVideoTrack?: MediaStreamTrack;
    remoteAudioTrack?: MediaStreamTrack;
    dataChannel?: RTCDataChannel;
    remoteStream?: MediaStream;
    videoRef: MutableRefObject<HTMLVideoElement | null>;
}

export const hashOfString = (str: string) => {
    let hash = 0,
        i,
        chr;
    for (i = 0; i < str.length; i++) {
        chr = str.charCodeAt(i);
        hash = (hash << 5) - hash + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};

export const shouldOfferConnection = (userUUID: string, peerUUID: string) => {
    return hashOfString(userUUID) > hashOfString(peerUUID);
};

export const establishWebRtcConnection = async (
    ws: WebSocket,
    userUUID: string,
    peerUUID: string
) => {
    const connection = new RTCPeerConnection(configs);
    const dataChannel = connection.createDataChannel("chatChannel");
    const iceCandidateQueue: RTCIceCandidate[] = [];
    const iceCandidateSendQueue: RTCIceCandidate[] = [];
    let answerSet = false;
    dataChannel.binaryType = "arraybuffer";
    let selfMediaStream;
    try {
        selfMediaStream = await navigator.mediaDevices.getUserMedia({
            video: true,
            audio: true,
        });
    } catch (e) {
        selfMediaStream = null;
    }
    const selfAudioTracks = selfMediaStream?.getAudioTracks();
    const selfAudioTrack = selfAudioTracks && selfAudioTracks[0];
    const selfVideoTracks = selfMediaStream?.getVideoTracks();
    const selfVideoTrack = selfVideoTracks && selfVideoTracks[0];
    selfAudioTrack && connection.addTrack(selfAudioTrack);
    selfVideoTrack && connection.addTrack(selfVideoTrack);

    const sendCandidate = async (candidate: RTCIceCandidate) => {
        const message: IWSMessage = {
            command: "WEB_RTC_ICE_CANDIDATE",
            args: {
                senderUUID: userUUID,
                receiverUUID: peerUUID,
                candidate: candidate,
            },
        };
        await waitForResponse(ws, message, "WEB_RTC_ICE_CANDIDATE_RESPONSE");
    };

    const tryAddCandidate = async (candidate: RTCIceCandidate) => {
        try {
            await connection.addIceCandidate(new RTCIceCandidate(candidate));
        } catch (e) {
            console.log("candidate error:", e);
        }
    };

    // const waitForIce = new Promise((r) => {
    //     connection.onicegatheringstatechange = (e) => {
    //         console.log(e);
    //         if (connection.iceGatheringState !== "complete") return;
    //         console.log("ice complete");
    //         r(connection.localDescription);
    //     };
    // });

    ws.addEventListener("message", async (e) => {
        const message: IWSMessage = JSON.parse(e.data);
        if (message.command === "WEB_RTC_ICE_CANDIDATE") {
            const { senderUUID, receiverUUID, candidate } = message.args;
            if (senderUUID === peerUUID && receiverUUID === userUUID) {
                console.log("got ice candidate", candidate);
                if (!answerSet) {
                    iceCandidateQueue.push(candidate);
                } else {
                    await tryAddCandidate(candidate);
                }
            }
        }
    });
    connection.onicecandidate = async (e) => {
        if (e.candidate !== null) {
            if (!answerSet) {
                iceCandidateSendQueue.push(e.candidate);
            } else {
                await sendCandidate(e.candidate);
            }
        }
    };

    connection.onicecandidateerror = (e) => {
        console.log("Ice Candidate Error: ", e);
    };

    connection.onnegotiationneeded = (e) => {
        console.log(e);
    };

    const offer = await connection.createOffer({
        offerToReceiveAudio: true,
        offerToReceiveVideo: true,
    });
    await connection.setLocalDescription(offer);
    // const realOffer = await waitForIce;
    const message: IWSMessage = {
        command: "WEB_RTC_OFFER",
        args: {
            senderUUID: userUUID,
            receiverUUID: peerUUID,
            offer: offer,
        },
    };
    const reply = await waitForResponse(ws, message, "WEB_RTC_ANSWER");
    const { answer } = reply.args;
    console.log(answer);
    console.log(connection);

    const waitForDataChannelEstablished = new Promise((r) => {
        dataChannel.onopen = (e) => {
            console.log(e);
            r(null);
        };
    });
    const waitForRemoteVideoTrackEstablished: Promise<MediaStreamTrack> = new Promise(
        (r) => {
            connection.ontrack = (e) => {
                const track = e.track;
                console.log(e);
                if (track.kind === "video") {
                    r(track);
                }
            };
        }
    );
    const waitForRemoteAudioTrackEstablished: Promise<MediaStreamTrack> = new Promise(
        (r) => {
            const handler = (e: RTCTrackEvent) => {
                const track = e.track;
                console.log(e);
                if (track.kind === "audio") {
                    r(track);
                    connection.removeEventListener("track", handler);
                }
            };

            connection.addEventListener("track", handler);
        }
    );
    await connection.setRemoteDescription(answer);
    answerSet = true;
    for (const candidate of iceCandidateQueue) {
        await tryAddCandidate(candidate);
    }

    for (const candidate of iceCandidateSendQueue) {
        await sendCandidate(candidate);
    }

    const remoteVideoTrack = await waitForRemoteVideoTrackEstablished;
    console.log("video");
    const remoteAudioTrack = await waitForRemoteAudioTrackEstablished;
    console.log("audio");

    setTimeout(() => {
        try {
            dataChannel.send("test");
        } catch (e) {
            console.log(e);
        }
    }, 1000);

    await waitForDataChannelEstablished;
    console.log("data");
    const remoteStream = new MediaStream();
    remoteStream.addTrack(remoteVideoTrack);
    remoteStream.addTrack(remoteAudioTrack);
    console.log("bigpepe");
    const webRtcConnection = {
        peerConnection: connection,
        dataChannel,
        remoteVideoTrack,
        remoteAudioTrack,
        remoteStream,
        videoRef: createRef<null>(),
    };
    console.log(webRtcConnection);
    return webRtcConnection;
};

export const respondToOffer = async (
    ws: WebSocket,
    offer: RTCSessionDescriptionInit,
    userUUID: string,
    senderUUID: string
) => {
    const connection = new RTCPeerConnection(configs);
    let selfMediaStream;
    try {
        selfMediaStream = await navigator.mediaDevices.getUserMedia({
            video: true,
            audio: true,
        });
    } catch (e) {
        selfMediaStream = null;
    }
    const selfAudioTracks = selfMediaStream?.getAudioTracks();
    const selfAudioTrack = selfAudioTracks && selfAudioTracks[0];
    const selfVideoTracks = selfMediaStream?.getVideoTracks();
    const selfVideoTrack = selfVideoTracks && selfVideoTracks[0];
    selfAudioTrack && connection.addTrack(selfAudioTrack);
    selfVideoTrack && connection.addTrack(selfVideoTrack);

    const iceCandidateQueue: RTCIceCandidate[] = [];
    const iceCandidateSendQueue: RTCIceCandidate[] = [];
    let answerSet = false;

    const tryAddCandidate = async (candidate: RTCIceCandidate) => {
        try {
            await connection.addIceCandidate(new RTCIceCandidate(candidate));
        } catch (e) {
            console.log("candidate error:", e);
        }
    };

    const sendCandidate = async (candidate: RTCIceCandidate) => {
        const message: IWSMessage = {
            command: "WEB_RTC_ICE_CANDIDATE",
            args: {
                senderUUID: userUUID,
                receiverUUID: senderUUID,
                candidate: candidate,
            },
        };
        await waitForResponse(ws, message, "WEB_RTC_ICE_CANDIDATE_RESPONSE");
    };

    ws.addEventListener("message", async (e) => {
        const message: IWSMessage = JSON.parse(e.data);
        const ogSenderUUID = senderUUID;
        if (message.command === "WEB_RTC_ICE_CANDIDATE") {
            const { senderUUID, receiverUUID, candidate } = message.args;
            console.log(e);
            if (senderUUID === ogSenderUUID && receiverUUID === userUUID) {
                console.log("got ice candidate", candidate);
                if (!answerSet) {
                    iceCandidateQueue.push(candidate);
                } else {
                    await tryAddCandidate(candidate);
                }
            }
        }
    });

    connection.onicecandidateerror = (e) => {
        console.log("Ice Candidate Error: ", e);
    };

    connection.onnegotiationneeded = (e) => {
        console.log(e);
    };
    connection.onicecandidate = async (e) => {
        if (e.candidate !== null) {
            if (!answerSet) {
                iceCandidateSendQueue.push(e.candidate);
            } else {
                await sendCandidate(e.candidate);
            }
        }
    };

    const waitForDataChannelEstablished: Promise<RTCDataChannel> = new Promise(
        (r) => {
            connection.ondatachannel = (e) => {
                const channel = e.channel;
                console.log(e);
                if (channel.label === "chatChannel") {
                    r(e.channel as RTCDataChannel);
                }
            };
        }
    );
    const waitForRemoteVideoTrackEstablished: Promise<MediaStreamTrack> = new Promise(
        (r) => {
            const handler = (e: RTCTrackEvent) => {
                const track = e.track;
                if (track.kind === "video") {
                    console.log(e);
                    r(track);
                    connection.removeEventListener("track", handler);
                }
            };

            connection.addEventListener("track", handler);
        }
    );
    const waitForRemoteAudioTrackEstablished: Promise<MediaStreamTrack> = new Promise(
        (r) => {
            const handler = (e: RTCTrackEvent) => {
                const track = e.track;
                if (track.kind === "audio") {
                    console.log(e);
                    r(track);
                    connection.removeEventListener("track", handler);
                }
            };

            connection.addEventListener("track", handler);
        }
    );

    await connection.setRemoteDescription(offer);
    const answer = await connection.createAnswer();
    await connection.setLocalDescription(answer);

    const answerMessage: IWSMessage = {
        command: "WEB_RTC_ANSWER",
        args: {
            senderUUID: userUUID,
            receiverUUID: senderUUID,
            answer: answer,
        },
    };
    const reply = await waitForResponse(
        ws,
        answerMessage,
        "WEB_RTC_ANSWER_RESPONSE"
    );

    answerSet = true;

    for (const candidate of iceCandidateQueue) {
        await tryAddCandidate(candidate);
    }

    for (const candidate of iceCandidateSendQueue) {
        await tryAddCandidate(candidate);
    }

    const { success, err } = reply.args;
    if (!success) {
        throw err;
    }

    const remoteVideoTrack = await waitForRemoteVideoTrackEstablished;
    const remoteAudioTrack = await waitForRemoteAudioTrackEstablished;
    const dataChannel: RTCDataChannel = await waitForDataChannelEstablished;
    const remoteStream = new MediaStream();
    remoteStream.addTrack(remoteVideoTrack);
    remoteStream.addTrack(remoteAudioTrack);
    const webRtcConnection = {
        peerConnection: connection,
        dataChannel: dataChannel,
        remoteVideoTrack: remoteVideoTrack,
        remoteAudioTrack: remoteAudioTrack,
        remoteStream: remoteStream,
        videoRef: createRef<null>(),
    };
    return webRtcConnection;
};
