import { WebRtcConnection } from "./webrtc-utils";

export interface IChatMessage {
    senderUUID: string;
    message: string;
}

export const broadcastChatMessage = (
    webRtcConnections: WebRtcConnection[],
    message: IChatMessage
) => {
    for (const connection of webRtcConnections) {
        if (!connection.dataChannel) continue;
        if (connection.dataChannel.readyState !== "open") continue;
        connection.dataChannel.send(JSON.stringify(message));
    }
};
