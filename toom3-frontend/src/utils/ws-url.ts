// const devURL = "ws://localhost:23143/";

const devURL = "wss://toom3.tong.icu";

export const wsUrl = () => {
    if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
        return devURL;
    } else {
        return `wss://${window.location.host}/`;
    }
};
