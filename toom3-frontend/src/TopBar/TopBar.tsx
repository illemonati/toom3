import { AppBar, Toolbar, Typography } from "@material-ui/core";
import React from "react";

export const TopBarComponent: React.FC = () => {
    return (
        <AppBar position="sticky">
            <Toolbar>
                <Typography variant="h6">Toom3</Typography>
            </Toolbar>
        </AppBar>
    );
};

export default TopBarComponent;
