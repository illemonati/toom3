import {
    Container,
    Grid,
    IconButton,
    Paper,
    TextField,
    Typography,
} from "@material-ui/core";
import React, { createRef, MutableRefObject, useEffect, useState } from "react";
import { broadcastChatMessage, IChatMessage } from "../../utils/chat-utils";
import "./RoomViewChatBox.css";
import SendIcon from "@material-ui/icons/Send";
import { WebRtcConnection } from "../../utils/webrtc-utils";

interface IRoomViewChatBoxComponentProps {
    userUUID: string;
    webRtcConnections: MutableRefObject<{ [key: string]: WebRtcConnection }>;
    promptUpdateChannels: (func: () => void) => void;
}

export const RoomViewChatBoxComponent: React.FC<IRoomViewChatBoxComponentProps> = (
    props: IRoomViewChatBoxComponentProps
) => {
    const [chatHistory, setChatHistory] = useState<IChatMessage[]>([]);
    const [height, setHeight] = useState<number>(0);
    const [messageInputValue, setMessageInputValue] = useState<string>("");
    const roomViewChatBoxChats = createRef<HTMLDivElement>();

    useEffect(() => {
        props.promptUpdateChannels(handleMessageReceive);
        const updateHeight = () => {
            const top = document
                .querySelector(".ParticipantWindows")
                ?.getBoundingClientRect().bottom;
            const bottom = document
                .querySelector(".RoomViewBottomBar")
                ?.getBoundingClientRect().top;
            if (!top || !bottom) return;
            const height = 0.9 * (bottom - top);
            setHeight(() => height);
        };
        updateHeight();
        window.addEventListener("resize", updateHeight);
        updateHeight();
        handleMessageReceive();

        return () => {
            window.removeEventListener("resize", updateHeight);
        };
        // eslint-disable-next-line
    }, []);

    const handleMessageReceive = () => {
        function handleChannelMessageReceive(e: MessageEvent) {
            const message = JSON.parse(e.data) as IChatMessage;
            setChatHistory((messages) => {
                return [...messages, message];
            });
        }
        for (const connection of Object.values(
            props.webRtcConnections.current
        )) {
            if (!connection.dataChannel) continue;
            connection.dataChannel.onmessage = handleChannelMessageReceive;
        }
    };

    useEffect(() => {
        if (!roomViewChatBoxChats.current) return;
        roomViewChatBoxChats.current.scrollTop =
            roomViewChatBoxChats.current.scrollHeight;
    }, [chatHistory, roomViewChatBoxChats]);

    const handleMessageSend = () => {
        const message: IChatMessage = {
            message: messageInputValue,
            senderUUID: props.userUUID,
        };
        setChatHistory((history) => {
            return [...history, message];
        });
        broadcastChatMessage(
            Object.values(props.webRtcConnections.current),
            message
        );
        setMessageInputValue(() => "");
    };

    const handleMessageInputKeyDown: React.KeyboardEventHandler<any> = (e) => {
        if (e.key === "Enter") {
            if (e.shiftKey) {
                setMessageInputValue((val) => val + "\n");
            } else {
                handleMessageSend();
            }
        }
    };

    return (
        <Paper className="RoomViewChatBox" style={{ height: height }}>
            <Container style={{ height: height }}>
                <div
                    className="RoomViewChatBoxChats"
                    ref={roomViewChatBoxChats}
                >
                    {chatHistory.map((message: IChatMessage, i) => {
                        return (
                            <Typography variant="body2" key={i}>
                                {`${message.senderUUID}: ${message.message}`}
                            </Typography>
                        );
                    })}
                </div>
                <Grid className="RoomViewChatBoxInputs" container spacing={2}>
                    <Grid item xs={10}>
                        <TextField
                            variant="outlined"
                            value={messageInputValue}
                            onChange={(e) =>
                                setMessageInputValue(() => e.target.value)
                            }
                            multiline
                            onKeyDown={handleMessageInputKeyDown}
                            rowsMax={2}
                            placeholder="Send Message..."
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <IconButton onClick={handleMessageSend}>
                            <SendIcon />
                        </IconButton>
                    </Grid>
                </Grid>
            </Container>
        </Paper>
    );
};

export default RoomViewChatBoxComponent;
