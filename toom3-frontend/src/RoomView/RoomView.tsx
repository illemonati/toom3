import React, { useEffect, useRef, useState } from "react";
import { useParams } from "react-router";
import RoomViewBottomBarComponent from "./RoomViewBottomBar/RoomViewBottomBarComponent";
import "./RoomView.css";
import { IWSMessage } from "../utils/ws-messages";
import {
    createRoom,
    getUserUUID,
    joinRoom,
    leaveRoom,
} from "../utils/ws-utils";
import { wsUrl } from "../utils/ws-url";
import { Box, Grid } from "@material-ui/core";
import ParticipantWindowsComponent from "./ParticipantWindows/ParticipantWindows";
import {
    establishWebRtcConnection,
    respondToOffer,
    shouldOfferConnection,
    WebRtcConnection,
} from "../utils/webrtc-utils";
import RoomViewChatBoxComponent from "./RoomViewChatBox/RoomViewChatBox";
import ParticipantWindowComponent from "./ParticipantWindows/ParticipantWindow/ParticipantWindow";

interface IRoomViewParams {
    id: string;
}

export const RoomViewComponent: React.FC = () => {
    const { id } = useParams<IRoomViewParams>();
    const userUUID = useRef<string>("user");
    const roomUUID = useRef<string>("room");
    const [participants, setParticipants] = useState<string[]>([]);
    const webRtcConnections = useRef<{ [key: string]: WebRtcConnection }>({});
    const ws = useRef<WebSocket | null>(null);
    const updateChatChannels = useRef<() => void>(() => {});
    const [
        currentSelectedParticipant,
        setCurrentSelectedParticipant,
    ] = useState<string | null>(null);
    const [
        currentSelectedParticipantWebRtcConnection,
        // eslint-disable-next-line
        setCurrentSelectedParticipantWebRtcConnection,
    ] = useState<WebRtcConnection | null>(null);
    const updateWebRtcConnections = useRef<
        (connections: { [key: string]: WebRtcConnection }) => void
    >(() => {});

    const initWs = async () => {
        await new Promise((r) => {
            ws.current = new WebSocket(wsUrl());
            ws.current.onopen = r;
        });
    };

    const initUserUUID = async () => {
        if (!ws.current) return;
        const uuid = await getUserUUID(ws.current);
        userUUID.current = uuid;
        return uuid;
    };

    const initRoom = async (userUUID: string) => {
        if (!ws.current || !userUUID) return;
        let newRoomUUID = "";
        if (id) {
            newRoomUUID = await joinRoom(ws.current, userUUID, id);
        } else {
            newRoomUUID = await createRoom(ws.current, userUUID);
        }
        roomUUID.current = newRoomUUID;
        handleRoomUUIDChange();
        return newRoomUUID;
    };

    const listenToParticipantChange = () => {
        if (!ws.current) return;
        ws.current.addEventListener("message", (e) => {
            const message = JSON.parse(e.data) as IWSMessage;
            if (message.command === "UPDATE_PARTICIPANTS") {
                setParticipants(() => message.args["participants"] as string[]);
            }
        });
    };
    const setNoOverflow = () => {
        document.body.style.overflow = "hidden";
    };

    const handleRoomUUIDChange = () => {
        if (roomUUID.current === "room") return;
        window.history.replaceState(
            null,
            `room: ${roomUUID}`,
            `${window.location.origin}/room/${roomUUID.current}`
        );
        document.title = `toom3: ${roomUUID.current}`;
        console.log(document.title);
    };

    const listenToIncomingWebRtcConnections = () => {
        if (!ws.current) return;
        ws.current.addEventListener("message", async (e) => {
            const message = JSON.parse(e.data) as IWSMessage;
            if (message.command === "WEB_RTC_OFFER") {
                if (!ws.current) return;
                const { offer, senderUUID } = message.args;
                const webRtcConnection = await respondToOffer(
                    ws.current,
                    offer,
                    userUUID.current,
                    senderUUID
                );
                webRtcConnections.current[senderUUID] = webRtcConnection;
                console.log(webRtcConnections.current);

                updateChatChannels.current();
                updateWebRtcConnections.current(webRtcConnections.current);
            }
        });
    };

    const handleSelectedParticipantChange = (participant: string) => {
        setCurrentSelectedParticipant(participant);
        setCurrentSelectedParticipantWebRtcConnection(
            webRtcConnections.current[participant]
        );
    };

    useEffect(() => {
        setNoOverflow();
        initWs()
            .then(listenToParticipantChange)
            .then(initUserUUID)
            .then(async (uuid) => await initRoom(uuid!))
            .then(listenToIncomingWebRtcConnections)
            .then(() => setCurrentSelectedParticipant(() => userUUID.current));

        window.addEventListener("beforeunload", () => {
            if (ws.current) leaveRoom(ws.current, userUUID.current);
        });
        return () => {
            if (ws.current) leaveRoom(ws.current, userUUID.current);
        };
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        console.log(participants);
        (async () => {
            if (!webRtcConnections.current) return;
            if (!ws.current) return;
            for (const participant of participants) {
                if (
                    !webRtcConnections.current[participant] &&
                    shouldOfferConnection(userUUID.current, participant)
                ) {
                    const webRtcConnection = await establishWebRtcConnection(
                        ws.current,
                        userUUID.current,
                        participant
                    );
                    webRtcConnections.current[participant] = webRtcConnection;
                }
            }
            for (const uuid of Object.keys(webRtcConnections)) {
                if (!participants.includes(uuid)) {
                    delete webRtcConnections.current[uuid];
                }
            }

            updateChatChannels.current();
            updateWebRtcConnections.current(webRtcConnections.current);
            console.log(webRtcConnections.current);
        })();
    }, [participants]);

    return (
        <div className="RoomView">
            <ParticipantWindowsComponent
                participants={participants}
                webRtcConnections={webRtcConnections.current}
                userUUID={userUUID.current}
                updateWebRtcConnections={(f) =>
                    (updateWebRtcConnections.current = f)
                }
                setSelectedParticipantFunction={handleSelectedParticipantChange}
            ></ParticipantWindowsComponent>
            <Box mt={3}>
                <Grid container>
                    <Grid item xs={9}>
                        {currentSelectedParticipant && (
                            <ParticipantWindowComponent
                                userUUID={userUUID.current}
                                participant={currentSelectedParticipant}
                                isSelectedView={true}
                                webRtcConnection={
                                    currentSelectedParticipantWebRtcConnection ||
                                    undefined
                                }
                            />
                        )}
                    </Grid>
                    <Grid item xs={3}>
                        <RoomViewChatBoxComponent
                            userUUID={userUUID.current}
                            webRtcConnections={webRtcConnections}
                            promptUpdateChannels={(f) =>
                                (updateChatChannels.current = f)
                            }
                        />
                    </Grid>
                </Grid>
            </Box>

            <RoomViewBottomBarComponent userUUID={userUUID.current} />
        </div>
    );
};

export default RoomViewComponent;
