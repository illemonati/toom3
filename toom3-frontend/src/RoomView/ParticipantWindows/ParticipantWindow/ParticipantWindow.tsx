import { Paper, Typography } from "@material-ui/core";
import React, { useEffect, useRef, useState } from "react";
import { WebRtcConnection } from "../../../utils/webrtc-utils";
import "./ParticipantWindow.css";

interface IParticipantWindowProps {
    participant: string;
    userUUID: string;
    webRtcConnection?: WebRtcConnection;
    isSelectedView?: boolean;
    setSelectedParticipantFunction?: (participant: string) => void;
}

export const ParticipantWindowComponent: React.FC<IParticipantWindowProps> = (
    props: IParticipantWindowProps
) => {
    const [width, setWidth] = useState<number | string>(0);
    const [height, setHeight] = useState<number | string>("100%");
    const isSelf = props.userUUID === props.participant;
    const selfVideoRef = useRef<HTMLVideoElement | null>(null);
    const selfRef = useRef<HTMLDivElement | null>(null);

    const handleClick = () => {
        if (props.isSelectedView) return;
        if (!props.setSelectedParticipantFunction) return;
        props.setSelectedParticipantFunction(props.participant);
    };

    useEffect(() => {
        let setSizeFunction: () => void;
        if (!props.isSelectedView) {
            setSizeFunction = () => {
                if (!selfRef.current) return;
                const height = selfRef.current.parentElement?.getBoundingClientRect()
                    .height;
                if (!height) return;
                setWidth(() => (height * 16) / 9);
                setHeight(() => "100%");
            };
        } else {
            setSizeFunction = () => {
                const top = document
                    .querySelector(".ParticipantWindows")
                    ?.getBoundingClientRect().bottom;
                const bottom = document
                    .querySelector(".RoomViewBottomBar")
                    ?.getBoundingClientRect().top;
                if (!top || !bottom) return;
                const height = 0.9 * (bottom - top);
                setWidth(() => "100%");
                setHeight(() => height);
            };
        }

        window.addEventListener("resize", setSizeFunction);
        setSizeFunction();

        console.log("rerender", props.webRtcConnection);
        if (
            props.webRtcConnection?.videoRef.current &&
            props.webRtcConnection?.remoteStream
        ) {
            const video = props.webRtcConnection.videoRef.current;
            video.srcObject = props.webRtcConnection.remoteStream;
            video.play();
        } else if (isSelf) {
            (async () => {
                if (!selfVideoRef.current) return;
                try {
                    const stream = await navigator.mediaDevices.getUserMedia({
                        video: true,
                        audio: false,
                    });
                    selfVideoRef.current.srcObject = stream;
                } catch (e) {}
                selfVideoRef.current.muted = true;
                // await selfVideoRef.current.play();
            })();
        }
        return () => {
            window.removeEventListener("resize", setSizeFunction);
        };
    }, [props, isSelf]);
    return (
        <Paper
            className="ParticipantWindow"
            variant="outlined"
            ref={selfRef}
            onClick={handleClick}
            style={{
                width: width,
                minWidth: width,
                height: height,
                minHeight: height,
            }}
        >
            {props.webRtcConnection || isSelf ? (
                <video
                    autoPlay={true}
                    className="ParticipantWindowVideo"
                    ref={
                        isSelf ? selfVideoRef : props.webRtcConnection!.videoRef
                    }
                ></video>
            ) : (
                <Typography variant="body2">
                    User: {props.participant}
                </Typography>
            )}
        </Paper>
    );
};

export default ParticipantWindowComponent;
