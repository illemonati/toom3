import { Paper } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { WebRtcConnection } from "../../utils/webrtc-utils";
import ParticipantWindowComponent from "./ParticipantWindow/ParticipantWindow";
import "./ParticipantWindows.css";

interface IParticipantWindowsComponentProps {
    participants: string[];
    webRtcConnections: { [key: string]: WebRtcConnection };
    userUUID: string;
    updateWebRtcConnections: (
        func: (connections: { [key: string]: WebRtcConnection }) => void
    ) => void;
    setSelectedParticipantFunction: (selectedParticipant: string) => void;
}

export const ParticipantWindowsComponent: React.FC<IParticipantWindowsComponentProps> = (
    props: IParticipantWindowsComponentProps
) => {
    const [webRtcConnections, setWebRtcConnections] = useState(
        props.webRtcConnections
    );

    useEffect(() => {
        props.updateWebRtcConnections((connections) => {
            console.log("got connections", connections);
            setWebRtcConnections(() => {
                return { ...connections };
            });
        });
    }, [props]);

    return (
        <Paper className="ParticipantWindows">
            {props.participants.map((participant, i) => {
                console.log(participant, i);
                return (
                    <ParticipantWindowComponent
                        participant={participant}
                        userUUID={props.userUUID}
                        setSelectedParticipantFunction={
                            props.setSelectedParticipantFunction
                        }
                        webRtcConnection={webRtcConnections[participant]}
                        key={i}
                    />
                );
            })}
        </Paper>
    );
};

export default ParticipantWindowsComponent;
