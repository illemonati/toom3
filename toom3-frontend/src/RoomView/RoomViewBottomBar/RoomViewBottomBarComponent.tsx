import { AppBar, Toolbar, Typography } from "@material-ui/core";
import React from "react";
import "./RoomViewBottomBar.css";

interface IRoomViewBottomBarProps {
    userUUID: string;
}

export const RoomViewBottomBarComponent: React.FC<IRoomViewBottomBarProps> = (
    props: IRoomViewBottomBarProps
) => {
    return (
        <AppBar
            className="RoomViewBottomBar"
            position="fixed"
            color="secondary"
        >
            <Toolbar>
                <Typography variant="body2">{props.userUUID}</Typography>
            </Toolbar>
        </AppBar>
    );
};

export default RoomViewBottomBarComponent;
