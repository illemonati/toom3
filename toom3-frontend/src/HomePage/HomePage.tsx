import {
  Box,
  Card,
  CardActionArea,
  CardContent,
  Container,
  Grid,
  Typography,
} from "@material-ui/core";
import React from "react";
import "./HomePage.css";
import { Link as RouterLink } from "react-router-dom";

export const HomePageComponent: React.FC = () => {
  return (
    <div className="HomePage" style={{ transform: `translateY(100%)` }}>
      <Box mt={4}>
        <Container maxWidth="lg">
          <Typography variant="h4">Welcome to Toom3</Typography>

          <Box mt={4} justifyContent="center" alignItems="center">
            <Grid container spacing={4}>
              <Grid item xs={6}>
                <Card>
                  <CardActionArea component={RouterLink} to="/room">
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                        Create a Room
                      </Typography>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        Create a room and get a unique room id for all your
                        friends to join!
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
              <Grid item xs={6}>
                <Card>
                  <CardActionArea disabled={true}>
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                        Join a Room
                      </Typography>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        Simply Paste URL in address bar to Join a room
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </Box>
    </div>
  );
};

export default HomePageComponent;
