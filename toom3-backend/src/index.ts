import express from "express";
import { RoomsHandler } from "./rooms";
import { expressApp, wss, server } from "./server";

const port = parseInt(process.env.TOOM3_BACKEND_PORT || "") || 23143;

const main = async () => {
    const roomsHandler = new RoomsHandler(wss);
    roomsHandler.start();
    expressApp.use(express.static("../toom3-frontend/build"));
    expressApp.use((req, res) => {
        res.status(200);
        res.sendFile("index.html", { root: "../toom3-frontend/build/" });
    });

    server.listen(port);
    console.log(`server listening on ${port}`);
};

main().then();
