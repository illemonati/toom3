export type Request =
    | "REQUEST_UUID"
    | "REQUEST_JOIN_ROOM"
    | "REQUEST_LEAVE_ROOM"
    | "REQUEST_PARTICIPANTS"
    | "REQUEST_CREATE_ROOM"
    | "WEB_RTC_OFFER"
    | "WEB_RTC_ANSWER"
    | "WEB_RTC_ICE_CANDIDATE";
export type Response =
    | "ISSUE_USER_UUID"
    | "ISSUE_ROOM_UUID"
    | "JOIN_ROOM_RESPONSE"
    | "LEAVE_ROOM_RESPONSE"
    | "UPDATE_PARTICIPANTS"
    | "WEB_RTC_OFFER_RESPONSE"
    | "WEB_RTC_ANSWER_RESPONSE"
    | "WEB_RTC_ICE_CANDIDATE_RESPONSE";

export type Command = Request | Response;

export interface IWSMessage {
    command: Command;
    args: any;
}
