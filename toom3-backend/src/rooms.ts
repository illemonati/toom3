import WebSocket from "ws";
import { v4 as uuidv4 } from "uuid";
import { IWSMessage } from "./ws-messages";
import { Room } from "./room";
import { IUser } from "./user";

interface IRoomMap {
    [key: string]: Room;
}

export class RoomsHandler {
    wss: WebSocket.Server;
    rooms: IRoomMap;
    constructor(wss: WebSocket.Server) {
        this.wss = wss;
        this.rooms = {};
    }

    handleRequestUUID(ws: WebSocket) {
        const newUUID = uuidv4();
        const reply: IWSMessage = {
            command: "ISSUE_USER_UUID",
            args: {
                uuid: newUUID,
            },
        };
        // console.log(newUUID);
        ws.send(JSON.stringify(reply));
    }

    handleRequestCreateRoom(ws: WebSocket, message: IWSMessage) {
        const uuid = message.args["userUUID"];
        const host: IUser = {
            uuid,
            ws,
        };
        const newRoom = new Room(host);

        this.rooms[newRoom.uuid] = newRoom;
        const reply: IWSMessage = {
            command: "ISSUE_ROOM_UUID",
            args: {
                uuid: newRoom.uuid,
            },
        };
        ws.send(JSON.stringify(reply));
        newRoom.startHandler(host);
        newRoom.broadcastParticipants();
    }

    handleRequestJoinRoom(ws: WebSocket, message: IWSMessage) {
        const { userUUID, roomUUID } = message.args;
        const user = {
            uuid: userUUID,
            ws,
        };
        const room = this.rooms[roomUUID];
        const reply: IWSMessage = {
            command: "JOIN_ROOM_RESPONSE",
            args: {},
        };
        if (!room) {
            reply.args = {
                success: false,
                err: "Room not found",
            };
        } else if (room.participants[userUUID]) {
            reply.args = {
                success: false,
                err: "Duplicate UUID",
            };
        } else {
            room.addUser(user);
            room.startHandler(user);
            reply.args = {
                success: true,
                roomUUID: room.uuid,
                err: null,
            };
        }
        ws.send(JSON.stringify(reply));
    }

    start() {
        this.wss.addListener("connection", (ws) => {
            ws.addEventListener("message", (e) => {
                // console.log(e.data);
                const handlers = {
                    REQUEST_UUID: this.handleRequestUUID.bind(this),
                    REQUEST_CREATE_ROOM: this.handleRequestCreateRoom.bind(
                        this
                    ),
                    REQUEST_JOIN_ROOM: this.handleRequestJoinRoom.bind(this),
                };
                try {
                    const message = JSON.parse(e.data) as IWSMessage;
                    const handlerFunction =
                        handlers[message.command as keyof typeof handlers];
                    handlerFunction && handlerFunction(ws, message);
                } catch (e) {
                    console.warn(e);
                }
            });
        });
    }
}
