import WebSocket from "ws";

export interface IUser {
    uuid: string;
    ws: WebSocket;
    roomHandler?: any;
}
