import { IUser } from "./user";
import { IWSMessage } from "./ws-messages";
import { v4 as uuidv4 } from "uuid";
import WebSocket from "ws";

export class Room {
    uuid: string;
    host: IUser;
    participants: {
        [key: string]: IUser;
    };
    constructor(host: IUser) {
        this.host = host;
        this.uuid = uuidv4();
        this.participants = {};
        // console.log(host);
        this.participants[host.uuid] = host;
        // console.log(this.participants);
    }

    startHandler(user: IUser) {
        const handlers = {
            REQUEST_PARTICIPANTS: this.handleRequestParticipants.bind(this),
            REQUEST_LEAVE_ROOM: this.handleRequestLeaveRoom.bind(this),
            WEB_RTC_OFFER: this.handleWebRtcOffer.bind(this),
            WEB_RTC_ANSWER: this.handleWebRtcAnswer.bind(this),
            WEB_RTC_ICE_CANDIDATE: this.handleWebRtcIceCandidate.bind(this),
        };

        const handler = (e: { data: string }) => {
            try {
                const message = JSON.parse(e.data) as IWSMessage;
                const handler =
                    handlers[message.command as keyof typeof handlers];
                handler && handler(user.ws, message);
            } catch (e) {
                console.warn(e);
            }
        };
        user.ws.addEventListener("message", handler);
        user.roomHandler = handler;
    }

    handleRequestParticipants(ws: WebSocket, message: IWSMessage) {
        const { userUUID } = message.args;
        const user = this.participants[userUUID];
        user && this.broadcastParticipants([user]);
    }

    handleWebRtcOffer(ws: WebSocket, message: IWSMessage) {
        const { senderUUID, receiverUUID, offer } = message.args;
        const replyToSender: IWSMessage = {
            command: "WEB_RTC_OFFER_RESPONSE",
            args: {
                success: false,
                err: null,
            },
        };
        const sender = this.participants[senderUUID];
        const receiver = this.participants[receiverUUID];
        if (!sender) {
            replyToSender.args.err = "Sender not found in room";
        } else if (!receiver) {
            replyToSender.args.err = "Receiver not found in room";
        } else if (sender.ws !== ws) {
            replyToSender.args.err = "ws did not match";
        } else {
            replyToSender.args.success = true;
            const messageToReceiver: IWSMessage = {
                command: "WEB_RTC_OFFER",
                args: {
                    senderUUID,
                    receiverUUID,
                    offer,
                },
            };
            receiver.ws.send(JSON.stringify(messageToReceiver));
        }
        ws.send(JSON.stringify(replyToSender));
    }
    handleWebRtcAnswer(ws: WebSocket, message: IWSMessage) {
        const { senderUUID, receiverUUID, answer } = message.args;
        // console.log(message.args);
        const replyToSender: IWSMessage = {
            command: "WEB_RTC_ANSWER_RESPONSE",
            args: {
                success: false,
                err: null,
            },
        };
        const sender = this.participants[senderUUID];
        const receiver = this.participants[receiverUUID];
        if (!sender) {
            replyToSender.args.err = "Sender not found in room";
        } else if (!receiver) {
            replyToSender.args.err = "Receiver not found in room";
        } else if (sender.ws !== ws) {
            replyToSender.args.err = "ws did not match";
        } else {
            replyToSender.args.success = true;
            const messageToReceiver: IWSMessage = {
                command: "WEB_RTC_ANSWER",
                args: {
                    senderUUID,
                    receiverUUID,
                    answer,
                },
            };
            receiver.ws.send(JSON.stringify(messageToReceiver));
        }
        ws.send(JSON.stringify(replyToSender));
    }
    handleWebRtcIceCandidate(ws: WebSocket, message: IWSMessage) {
        const { senderUUID, receiverUUID, candidate } = message.args;
        // console.log(message.args);
        const replyToSender: IWSMessage = {
            command: "WEB_RTC_ICE_CANDIDATE_RESPONSE",
            args: {
                success: false,
                err: null,
            },
        };
        const sender = this.participants[senderUUID];
        const receiver = this.participants[receiverUUID];
        if (!sender) {
            replyToSender.args.err = "Sender not found in room";
        } else if (!receiver) {
            replyToSender.args.err = "Receiver not found in room";
        } else if (sender.ws !== ws) {
            replyToSender.args.err = "ws did not match";
        } else {
            replyToSender.args.success = true;
            const messageToReceiver: IWSMessage = {
                command: "WEB_RTC_ICE_CANDIDATE",
                args: {
                    senderUUID,
                    receiverUUID,
                    candidate,
                },
            };
            receiver.ws.send(JSON.stringify(messageToReceiver));
        }
        ws.send(JSON.stringify(replyToSender));
    }

    handleRequestLeaveRoom(ws: WebSocket, message: IWSMessage) {
        const { userUUID } = message.args;
        const user = this.participants[userUUID];
        const reply: IWSMessage = {
            command: "LEAVE_ROOM_RESPONSE",
            args: {},
        };
        if (!user) {
            reply.args = {
                success: false,
                err: "User not found in room",
            };
        } else if (user.ws !== ws) {
            reply.args = {
                success: false,
                err: "ws did not match",
            };
        } else {
            reply.args = {
                success: true,
                err: null,
            };
            this.removeUser(user);
            user.roomHandler &&
                user.ws.removeEventListener("message", user.roomHandler);
        }
        ws.send(JSON.stringify(reply));
    }

    addUser(user: IUser) {
        this.participants[user.uuid] = user;
        this.broadcastParticipants();
    }

    removeUser(user: IUser) {
        delete this.participants[user.uuid];
        this.broadcastParticipants();
    }

    broadcastParticipants(usersToBroadcastTo?: IUser[]) {
        !usersToBroadcastTo &&
            (usersToBroadcastTo = Object.values(this.participants));
        const uuids = Object.keys(this.participants);
        const message: IWSMessage = {
            command: "UPDATE_PARTICIPANTS",
            args: {
                participants: uuids,
            },
        };
        const strmessage = JSON.stringify(message);
        for (const user of usersToBroadcastTo) {
            user.ws.send(strmessage);
        }
    }
}
