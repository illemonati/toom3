import express from "express";
import http from "http";
import WebSocket from "ws";

const expressApp = express();
const server = http.createServer(expressApp);
const wss = new WebSocket.Server({ server });

export { expressApp, server, wss };
