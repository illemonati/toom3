"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const rooms_1 = require("./rooms");
const server_1 = require("./server");
const port = parseInt(process.env.TOOM3_BACKEND_PORT || "") || 23143;
const main = async () => {
    const roomsHandler = new rooms_1.RoomsHandler(server_1.wss);
    roomsHandler.start();
    server_1.expressApp.use(express_1.default.static("../toom3-frontend/build"));
    server_1.expressApp.use((req, res) => {
        res.status(200);
        res.sendFile("index.html", { root: "../toom3-frontend/build/" });
    });
    server_1.server.listen(port);
    console.log(`server listening on ${port}`);
};
main().then();
